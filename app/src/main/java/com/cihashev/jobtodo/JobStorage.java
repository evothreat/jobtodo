package com.cihashev.jobtodo;

import java.util.ArrayList;

public class JobStorage {

    private static JobStorage jobStorage;
    private ArrayList<Job> undoneJobs;
    private ArrayList<Job> doneJobs;

    private JobStorage() {
        doneJobs = new ArrayList<>();
        undoneJobs = new ArrayList<>();
        // TODO: remove this test data
        undoneJobs.add(new Job("kick some asses"));
        undoneJobs.add(new Job("buy house"));
        undoneJobs.add(new Job("make children"));
        undoneJobs.add(new Job("drive car"));
        undoneJobs.add(new Job("learn android programming"));
        undoneJobs.add(new Job("go to doctor"));
        undoneJobs.add(new Job("make money"));
        undoneJobs.add(new Job("take child from school"));
        undoneJobs.add(new Job("make breakfast"));
    }

    public synchronized static JobStorage getInstance() {
        if (jobStorage == null) {
            jobStorage = new JobStorage();
        }
        return jobStorage;
    }

    public void removeJob(int i, JobGroup jg) {
        // TODO: throw exceptions on outbound range
        switch (jg) {
            case ALL_JOBS:
                if (i < undoneJobs.size()) {
                    undoneJobs.remove(i);
                } else {
                    doneJobs.remove(i-undoneJobs.size());
                }
                break;
            case DONE_JOBS:
                doneJobs.remove(i);
                break;
            case UNDONE_JOBS:
                undoneJobs.remove(i);
                break;
        }
    }

    public Job getJob(int i, JobGroup jg) {
        // TODO: throw exceptions on outbound range
        switch (jg) {
            case ALL_JOBS:
                if (i < undoneJobs.size()) {
                    return undoneJobs.get(i);
                } else {
                    return doneJobs.get(i-undoneJobs.size());
                }
            case DONE_JOBS:
                return doneJobs.get(i);
            case UNDONE_JOBS:
                return undoneJobs.get(i);
        }
        return null;
    }

    public void setJobDone(int i) {
        // TODO: throw exceptions on outbound range
        doneJobs.add(undoneJobs.get(i));
        undoneJobs.remove(i);
    }

    public void addNewJob(Job job, boolean isDone) {
        if (isDone) {
            doneJobs.add(job);
        } else {
            undoneJobs.add(job);
        }
    }

    public boolean isJobDone(int i, JobGroup jg) {
        // TODO: throw exceptions on outbound range
        if (jg == JobGroup.ALL_JOBS) {
            return i >= undoneJobs.size();
        }
        return jg == JobGroup.DONE_JOBS;
    }

    public int size(JobGroup jg) {
        // TODO: throw exceptions on outbound range
        switch (jg) {
            case ALL_JOBS:
                return doneJobs.size() + undoneJobs.size();
            case DONE_JOBS:
                return doneJobs.size();
            case UNDONE_JOBS:
                return undoneJobs.size();
        }
        return -1;
    }
}
