package com.cihashev.jobtodo;

public enum JobGroup {
    UNKNOWN(-1),
    ALL_JOBS(0),
    DONE_JOBS(1),
    UNDONE_JOBS(2);

    private int value;

    JobGroup(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    static JobGroup from(int value) {
        switch (value) {
            case 0:
                return ALL_JOBS;
            case 1:
                return DONE_JOBS;
            case 2:
                return UNDONE_JOBS;
        }
        return UNKNOWN;
    }
}
